package main

import (
	"crypto/md5"
	"encoding/hex"
	"github.com/urfave/cli"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"
)

type XmlParser struct {
	Hash string
	Body string
	CreatedAt time.Time
}

type JsonParser struct {
	Hash string
	Body string
	CreatedAt time.Time
}

type Parser interface {
	Validate()
	Save(f *os.File)
}

func (j *JsonParser) Save(f *os.File) {
	_, err := f.WriteString(j.Hash + ";")
	if err != nil {
		log.Println(err)
	}
}

func (j *JsonParser) Validate() {
	data, e := ioutil.ReadFile("json.txt")
	if e != nil {
		log.Println(e)
	}

	split := strings.Split(string(data), ";")
	for _, value := range split {
		if value == j.Hash {
			log.Println("Error, element already exist!", value)
			return
		}
	}

	f, err := os.OpenFile("json.txt", os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		panic(err)
	}
	defer func(f *os.File) {
		err := f.Close()
		if err != nil {
			log.Println(err)
		}
	}(f)

	j.Save(f)
}

func (x *XmlParser) Save(f *os.File) {
	_, err := f.WriteString(x.Hash + ";")
	if err != nil {
		log.Println(err)
	}
}

func (x *XmlParser) Validate() {
	data, e := ioutil.ReadFile("xml.txt")
	if e != nil {
		log.Println(e)
	}

	split := strings.Split(string(data), ";")
	for _, value := range split {
		//fmt.Println(len(value), "value")
		//fmt.Println(len(x.Hash), "hash")
		if value == x.Hash {
			log.Println("Error, element already exist!", value)
			return
		}
	}

	f, err := os.OpenFile("xml.txt", os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		panic(err)
	}
	defer func(f *os.File) {
		err := f.Close()
		if err != nil {
			log.Println(err)
		}
	}(f)

	x.Save(f)
}

var json string
var xml string

func main() {
	app := cli.NewApp()

	app.Flags = []cli.Flag {
		cli.StringFlag{
			Name: "json",
			Value: "",
			Usage: "language for the greeting",
			Destination: &json,
		},
		cli.StringFlag{
			Name: "xml",
			Value: "",
			Usage: "language for the greeting",
			Destination: &xml,
		},
	}

	HandleCommand(app)

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func HandleCommand(app *cli.App) {
	app.Action = func(c *cli.Context) error {
		if json != "" {
			jsonParser := Hash("json", json)
			jsonParser.Validate()
		} else {
			xmlParser := Hash("xml", xml)
			xmlParser.Validate()
		}

		return nil
	}
}

func Hash(typeOfValue, body string) Parser {
	if typeOfValue == "json" {
		hash := md5.Sum([]byte(body))
		jsonParser := JsonParser{hex.EncodeToString(hash[:]), body, time.Now()}
		return &jsonParser
	}
	if typeOfValue == "xml" {
		hash := md5.Sum([]byte(body))
		xmlParser := XmlParser{hex.EncodeToString(hash[:]), body, time.Now()}
		return &xmlParser
	}

	return nil
}
